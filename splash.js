import React, { useEffect} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native'; 


const Splash = ({navigation}) => {
    useEffect (() => {
        setTimeout(() => {
            navigation.replace('home');
        }, 4000);
    });
    return(
        <View style={styles.wrapper}>
            <View style={styles.screen}>
            <Image source={require('../aset/mawar.jpg')} style={{  width:110,height:100,}}></Image>
            <Image source={require('../aset/london.jpg')} style={{  width:110,height:100,}}></Image>
            <Image source={require('../aset/senja.jpg')} style={{  width:110,height:100,}}></Image>
            </View>
            <Text style={{fontSize:30,color:'black',fontWeight: 'bold',paddingBottom:20,marginLeft:25,marginTop:80,}}>Bunga Rampai</Text>
            <Text style={styles.welcomeText}>pribahasa </Text>
            <Text style={{fontSize:55,color:'maroon',fontWeight: 'bold',paddingBottom:20,marginLeft:130,marginTop: 10,}}>& pantun</Text>
            <View style={styles.foter}>
            <Image source={require('../aset/macan.jpg')} style={{  width:170,height:100,}}></Image>
            <Image source={require('../aset/pantai.jpeg')} style={{  width:160,height:100,}}></Image>
           </View>
        </View>
    );
};

const styles= StyleSheet.create({
    wrapper:{
        flex: 1,
        backgroundColor: 'white',
    },
    screen:{
        flexDirection:'row',
        marginHorizontal:15,
        paddingTop:20,
      },
      foter:{
        flexDirection:'row',
        marginHorizontal:15,
        paddingTop:100,
      },
    welcomeText: {
        fontSize:55,
        color:'maroon',
        fontWeight: 'bold',
        paddingBottom:5,
        marginLeft:50,
        marginTop:60,
    },
});

export default Splash;