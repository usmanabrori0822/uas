import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView} from 'react-native'

export default class pantun extends Component {
    render() {
        return (
            <View style={styles.countainer}>
                <ScrollView>
            <View  style={styles.page}>
                <Text style={{fontSize: 16,marginLeft:13,marginTop:5}}>Malam minggu ke pasar minggu</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Beli korma naik onta</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Jangan lupa shalat lima waktu</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Agar kita masuk surga</Text>
            </View>
            <View  style={styles.page}>
                <Text style={{fontSize: 16,marginLeft:13,marginTop:5}}>Burung merpati burung dara</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Hinggap di dahan pohon jati</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Kalau adik mencintai saya</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Mari kita ber ikrar janji</Text>
            </View>
            <View  style={styles.page}>
                <Text style={{fontSize: 16,marginLeft:13,marginTop:5}}>Pergi nelayan mencari ikan</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Jangan lupa membawa tali</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Bekerja memang melelahkan</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>jangan sampai lupa diri</Text>
            </View>
            <View  style={styles.page}>
                <Text style={{fontSize: 16,marginLeft:13,marginTop:5}}>Semua manusia punya jari </Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Bukan hanya berjumlah empat</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Berolah raga di pagi hari</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Membuat badan menjadi sehat</Text>
            </View>
            <View  style={styles.page}>
                <Text style={{fontSize: 16,marginLeft:13,marginTop:5}}>Ayah kepasar membeli sepatu</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Kemudian pergi ke taman</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Senang-senanglah kamu membantu</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Agar kamu dapat pahala</Text>
            </View>
            <View  style={styles.page}>
                <Text style={{fontSize: 16,marginLeft:13,marginTop:5}}>Anak bangau turun sepuluh</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Mati dua tinggal delapan</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Tuntutlah ilmu sungguh-sungguh</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Kelak kau tidak ketinggalan</Text>
            </View>
            <View  style={styles.page}>
                <Text style={{fontSize: 16,marginLeft:13,marginTop:5}}>Bepergian dengan sepeda</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Di jalan menemukan duri</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Belajarlah selagi muda</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Beruntung dikemudian hari</Text>
            </View>
            <View  style={styles.page}>
                <Text style={{fontSize: 16,marginLeft:13,marginTop:5}}>Hewan ini tidak berkaki</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Hewan itu memang berbisa</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Janganlah engkau suka memaki</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Itu perbuatan dosa</Text>
            </View>
            <View  style={styles.page}>
                <Text style={{fontSize: 16,marginLeft:13,marginTop:5}}>Rumput ilalang bergoyang-goyang</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Tertiup angin kesana-kesini</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Mari kitatunaikan sembahyang</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Bekal kita di akhirat nanti</Text>
            </View>
            <View  style={styles.page}>
                <Text style={{fontSize: 16,marginLeft:13,marginTop:5,}}>Buat apa punya sagu</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Kalo tidak bisa dimakan</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Buat apa punya ilmu</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>kalau tidak diamalkan</Text>
            </View>
            </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    countainer:{
        flex: 1,
        backgroundColor: 'papayawhip',
    },
    page: {
        height: 80,
        width: 260,
        marginTop: 15,
        backgroundColor: 'moccasin',
        borderRadius: 5,
        marginHorizontal: 50,
    }
})
 