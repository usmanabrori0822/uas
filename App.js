import React from 'react';
import { StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import Route from './src/route';

const App = () => {
  return (
    <NavigationContainer>
        <Route/>
    </NavigationContainer>
  );
};

export default App;

const styles = StyleSheet.create({});