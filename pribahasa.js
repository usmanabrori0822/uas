import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView } from 'react-native'

export default class pribahasa extends Component {
    render() {
        return (
            <View style={styles.countainer}>
                <ScrollView>
            <View  style={styles.page}>
                <Text style={{fontSize: 16,marginLeft:5,marginTop:5}}>1. Air Beriak Tanda Tak Dalam.</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Artinya :</Text>
                <Text style={{fontSize: 14,marginLeft:13}}>Orang yang terlalu banyak bicara biasanya bodoh/picak pengetahuan</Text>
            </View>
            <View  style={styles.page}>
                <Text style={{fontSize: 16,marginLeft:5,marginTop:5}}>2. Lempar Batu Sembunyi Tangan.</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Artinya :</Text>
                <Text style={{fontSize: 14,marginLeft:13}}>Tidak bertanggung jawab dengan perbuatan yang dilakukan</Text>
            </View>
            <View  style={styles.page}>
                <Text style={{fontSize: 16,marginLeft:5,marginTop:5}}>3. Berani Karna Benar Takut Karna Salah</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Artinya :</Text>
                <Text style={{fontSize: 14,marginLeft:13}}>Orang yang selalu ketakutan dalam melakukan kejahatan/kesalahan</Text>
            </View>
            <View  style={styles.page}>
                <Text style={{fontSize: 16,marginLeft:5,marginTop:5}}>4. Hitam-Hitam Gula Jawa.</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Artinya :</Text>
                <Text style={{fontSize: 14,marginLeft:13}}>Meskipun kulitnya hitam tapi wajahnya manis</Text>
            </View>
            <View  style={styles.page}>
                <Text style={{fontSize: 16,marginLeft:5,marginTop:5}}>5. Janji Ditepati, Ikrar Dimuliakan.</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Artinya :</Text>
                <Text style={{fontSize: 14,marginLeft:13}}>Janji adalah hutang jadi harus dibayar</Text>
            </View>
            <View  style={styles.page}>
                <Text style={{fontSize: 16,marginLeft:5,marginTop:5}}>6. Jiwa Bergantung Di Ujung Rambung.</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Artinya :</Text>
                <Text style={{fontSize: 14,marginLeft:13}}>Jiwanya terancam (dalam keadaan berbahaya)</Text>
            </View>
            <View  style={styles.page}>
                <Text style={{fontSize: 16,marginLeft:5,marginTop:5}}>7. Jimak-Jimak Merpati.</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Artinya :</Text>
                <Text style={{fontSize: 14,marginLeft:13}}>Kelihatannya mau, tapi setelah didekati malah lari</Text>
            </View>
            <View  style={styles.page}>
                <Text style={{fontSize: 16,marginLeft:5,marginTop:5}}>8. Bagai Kambing Dalam Biduk.</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Artinya :</Text>
                <Text style={{fontSize: 14,marginLeft:13}}>Orang yang takut dengan sesuatu, tapi tidak bisa menyelamatkan diri </Text>
            </View>
            <View  style={styles.page}>
                <Text style={{fontSize: 16,marginLeft:5,marginTop:5}}>9. Hemat Pangkal Kaya, Rajin Pangkal Pandai.</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Artinya :</Text>
                <Text style={{fontSize: 14,marginLeft:13}}>Jika ingin kaya berhematlah dan jika pandai, rajinlah belajar</Text>
            </View>
            <View  style={styles.page}>
                <Text style={{fontSize: 16,marginLeft:5,marginTop:5}}>10. Tidak Ada Laut Ynag Tak Berombak.</Text>
                <Text style={{fontSize: 16,marginLeft:13}}>Artinya :</Text>
                <Text style={{fontSize: 14,marginLeft:13}}>Setiap pekerjaan pasti ada bahayanya / kesulitannya</Text>
            </View>
            </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    countainer:{
        flex: 1,
        backgroundColor: 'papayawhip',
    },
    page: {
        height: 80,
        width: 330,
        marginTop: 15,
        backgroundColor: 'moccasin',
        borderRadius: 5,
        marginHorizontal: 10,
    }
})
 