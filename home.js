import React, {Component} from 'react'
import { Text, StyleSheet, View, Image, TouchableOpacity } from 'react-native'


export default class home extends Component {
    constructor (props) {
        super(props);
        this.state ={};
    }
    render() {
        return (
            <View style={styles.countainer}>
            <View style={styles.navbar}>
                <Text style={{fontWeight:'bold', fontSize:20, fontFamily:'times new roman', color: 'white', textAlign:'center', marginHorizontal:60}}> PRIBAHASA & PANTUN</Text>
            </View>
            <View style={styles.content}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('pribahasa')}>
                    <Image source={require('../aset/pribahasa.jpg')} style={{width:200,height:250, marginBottom:10, marginTop:50,marginHorizontal:80}}></Image>
                </TouchableOpacity> 
                <TouchableOpacity onPress={() => this.props.navigation.navigate('pantun')}>
                    <Image source={require('../aset/pantun.jpg')} style={{width:200,height:250, marginBottom:10, marginTop:50,marginHorizontal:80}}></Image>
                </TouchableOpacity>
            </View>
            </View>
        )
    }
}

const styles= StyleSheet.create({
    countainer:{
        flex:1,
        backgroundColor:'white',
    },
    navbar:{
        height: 60,
        backgroundColor:'black',
        elevation:3,
        paddingHorizontal:15,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
    },
    content:{

    }
})