import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Splash from '../componen/splash';
import Home from '../componen/home';
import Pribahasa from '../componen/pribahasa';
import Pantun from '../componen/pantun';

const Stack = createStackNavigator();

const Route = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="splash"
                component={Splash}
                options={{headerShown: false}} 
            />
            <Stack.Screen
                name="home"
                component={Home}
                options={{headerShown: false}} 
            />
            <Stack.Screen
                name="pribahasa"
                component={Pribahasa}
            />
            <Stack.Screen
                name="pantun"
                component={Pantun} 
            />
        </Stack.Navigator>
    );
};

export default Route;